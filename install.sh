#!/bin/bash

RED='\033[0;31m'
YELLOW="\033[1;33m"
GREEN="\033[0;32m"
NC='\033[0m' # No Color
cwd=${PWD}

shellrc="$HOME/.bashrc"

function verify_root() {
    if [[ $EUID -ne 0 ]]; then
        echo_fatal "Script must be ran as root"
    fi
}

function echo_warn() {
    echo -e "${YELLOW}WARN: $1 ${NC}"
}

function echo_info() {
    echo -e "${GREEN}INFO: $1 ${NC}"
}

function echo_error() {
    echo -e "${RED}ERROR: $1 ${NC}"
}

function echo_fatal() {
    echo -e "${RED}FATAL: $1 ${NC}"
    exit -1
}

function check_error() {
    if [ $? -ne 0 ] ; then
        echo_fatal "$1"
    fi
}

function workon_dl4cv() {
    echo_info "Selecting dl4cv environment"
    export WORKON_HOME=$HOME/.virtualenvs
    export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3
    source /usr/local/bin/virtualenvwrapper.sh
    workon dl4cv
    check_error "Failed to select dl4cv"
}

function usage() {
    echo "usage: $1 [-h|--help] [--skip-step-{1..4}] [--with-contrib]" 
    echo -e "\t-h|--help : Show this help"
    echo -e "\t--skip-step-{1..4} : Skip step n, where n is 1, 2, 3, or 4, can specify multiple"
    echo -e "\t--with-contrib     : install DL4CV with OpenCV contrib module"
    exit 0
}

# Arguments
skip_step_1=0
skip_step_2=0
skip_step_3=0
skip_step_4=0
with_contrib=0
opencv="3.4.1"

# Parsing command line arguments
for var in "$@" ; do
    case "$var" in
        "-h" | "--help" )
            usage $0 ;;
        "--skip-step-1" )
            skip_step_1=1 ;;
        "--skip-step-2" )
            skip_step_2=1 ;;
        "--skip-step-3" )
            skip_step_3=1 ;;
        "--skip-step-4" )
            skip_step_4=1 ;;
        "--with-contrib" )
            with_contrib=1 ;;
    esac
done

# Verify running root
verify_root

if [ "`basename $SHELL`" == "zsh" ] ; then
    echo_info "Setting up for ZSH shell"
    shellrc="$HOME/.zshrc"
fi

## Step 1
if [[ $skip_step_1 -eq 0 ]] ; then
    echo_info "Step 1 - Installing system dependencies"

    echo_info "Getting Linux version"
    linux_version=`cat /etc/lsb-release | sed "s/DISTRIB_RELEASE=\([0-9]*.[0-9]*\)/\1/g;t;d"`
    check_error "Failed to get Linux version"
    
    if [ ! -f "packages/$linux_version" ] ; then
        echo_fatal "Unknown Linux version: $linux_version"
    fi

    echo_info "Parsing Linux packages for version $linux_version"
    apt_packages=`cat packages/$linux_version | sed ':a;N;$!ba;s/\n/ /g'`
    check_error "Erro parsing Linux packages"

    echo_info "Updating apt package repos"
    apt update
    check_error "Failed to update apt package repos" 

    echo_info "Installing dependencies"
    apt install -y $apt_packages
    check_error "Failed to install apt packages"

    echo_info "Installing pip"
    wget https://bootstrap.pypa.io/get-pip.py
    check_error "Failed to download pip installation script"

    python3 get-pip.py
    check_error "Failed to install pip"

    echo_info "Installing virtualenv"
    pip install virtualenv virtualenvwrapper
    check_error "Failed to install virtualenv"
else
    echo_warn "Skipping step 1"
fi

### Step 2

if [[ $skip_step_2 -eq 0 ]] ; then
    echo_info "Step 2 - Setting up virtual environment"

    echo_info "Adding needed exports to $shellrc"
    echo -e "\n# virtualenv and virtualenvwrapper" >> $shellrc 
    echo "export WORKON_HOME=$HOME/.virtualenvs" >> $shellrc 
    echo "export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3" >> $shellrc 
    echo "source /usr/local/bin/virtualenvwrapper.sh" >> $shellrc 
    source $shellrc
    check_error "Failed to source updated $shellrc"

    WORKON_HOME=$HOME/.virtualenvs
    VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3
    source /usr/local/bin/virtualenvwrapper.sh

    echo_info "Creating virtual environment dl4cv (using python 3)"
    mkvirtualenv dl4cv -p python3
    check_error "Failed to create virtual environment"
    
    workon_dl4cv

    echo_info "Installing NumPy into virtual environment"
    pip install numpy
    check_error "Failed to install NumPy"
else
    echo_warn "Skipping step 2"
fi

if [ ! -f "get-pip.py" ] ; then
    echo_info "Cleaning up pip environment for virtualenv" 
    rm -rf get-pip.py
    check_error "Failed to clean up environment"
fi

### Step 3

if [[ $skip_step_3 -eq 0 ]] ; then
    echo_info "Step 3 - Compiling & installing OpenCV"
    cv_py_version=`python3 --version | sed "s/\(Python\s\)\([0-9]\)\.\([0-9]\)\.\([0-9]\)/\2\3m/g"`
    py_version=`python3 --version | sed "s/\(Python\s\)\([0-9]\)\.\([0-9]\)\.\([0-9]\)/python\2.\3/g"`

    if [ ! -f "opencv.zip" ] ; then
        echo_info "Downloading OpenCV version $opencv"
        wget -O opencv.zip https://github.com/Itseez/opencv/archive/$opencv.zip
        check_error "Failed to download OpenCV version $opencv"
    else
        echo_info "Already downloaded OpenCV"
    fi

    if [ ! -d "opencv-$opencv" ] ; then
        echo_info "Unzipping OpenCV library"
        unzip opencv.zip
        check_error "Failed to unzip OpenCV library"
    else
        echo_info "Already decompressed OpenCV"
    fi

    cmake_contrib=""
    
    if [[ $with_contrib -eq 1 ]] ; then 
        echo_warn "Building OpenCV with the contrib library"

        echo_info "Downloading OpenCV contrib source"
        wget -O opencv_contrib.zip https://github.com/Itseez/opencv_contrib/archive/$opencv.zip
        check_error "Failed to download OpenCV contrib source"

        echo_info "Unzipping OpenCV contrib library"
        unzip opencv_contrib.zip
        check_error "Failed to unzip OpenCV contrib library" 

        cmake_contrib="-D OPENCV_EXTRA_MODULES_PATH=./opencv_contrib-$opencv/modules"
    fi

    echo_info "Building OpenCV"

    cd opencv-$opencv
    
    if [ ! -d "build" ] ; then
        mkdir build
    fi

    cd build
    
    echo_info "Executing CMake"
    cmake -D CMAKE_BUILD_TYPE=RELEASE \
        -D CMAKE_INSTALL_PREFIX=/usr/local \
        $cmake_contrib \
        ..
    check_error "CMake for OpenCV failed"

    echo_info "Building OpenCV"
    make -j4
    check_error "Failed to compile OpenCV"

    echo_info "Installing OpenCV"
    make install
    check_error "Failed to install OpenCV"

    echo_info "Adding OpenCV to virtualenv"
    ln -s /usr/local/lib/$py_version/site-packages/cv2.cpython-$cv_py_version-x86_64-linux-gnu.so \
        ~/.virtualenvs/dl4cv/lib/$py_version/site-packages/cv2.so
    check_error "Failed to create symbolic link to OpenCV binary"
    
    # Changing directories back to the starting directory
    cd ../../
else
    echo_warn "Skipping step 3"
fi

### Step 4

if [[ $skip_step_4 -eq 0 ]] ; then
    echo_info "Step 4 - Installing Keras"

    if [[ $skip_step_2 -eq 1 ]] ; then
        workon_dl4cv
    fi

    echo_info "Installing Python dependencies into virtualenv"
    pip install -r requirements.txt
    check_error "Failed installing virtualenv Python dependencies"
    
    echo_info "Creating directory ~/.keras"
    mkdir ~/.keras
    check_error "Failed to create directory ~/.keras"

    echo_info "Creating ~/.keras/keras.json"
    cp keras.json ~/.keras/keras.json
    check_error "Failed to copy keras.json over"
else
    echo_warn "Skipping step 4"
fi

echo_info "Done."

